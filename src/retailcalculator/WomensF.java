/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package retailcalculator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;




public class WomensF extends JPanel implements ActionListener {

    Calculator c = new Calculator();
    private double runS = 80.00;
    private double hikingS = 100.00;
    private double regularS = 50.00;
    
    private String data;
    private String text;
    private String t;
    private String item;
    
    private JTextArea area = new JTextArea(text, 10, 15);
    private JRadioButton run = new JRadioButton("Running Shoes:");
    private JRadioButton hiking = new JRadioButton("Hiking Boots:");
    private JRadioButton regular = new JRadioButton("Dress Shoes:");
    private JLabel oPriceLabel = new JLabel("Orginial cost of Item");

    private JLabel mUpLabel = new JLabel("Mark Down Percent: ");
    private JButton calc = new JButton("Calculate");
    private JButton clear = new JButton("Clear Fields");
    private JTextField oPrice = new JTextField(10);
   
    
    private JTextField mUp = new JTextField(10);
    private JPanel rButtons = new JPanel();

    private JPanel nButtons = new JPanel();
    private JPanel tFields = new JPanel();
    private JPanel pricePanel = new JPanel();
    private JPanel tArea = new JPanel();
    public Container createPanelTwo() {
        nButtons.add(calc);
        nButtons.add(clear);
        nButtons.setLayout(new FlowLayout());
        Container b = new Container();
        b.setLayout(new BorderLayout(10, 10));
        b.add(nButtons, BorderLayout.SOUTH);
        
        return b;
    }

    public Container createPanelThree() {
        pricePanel.setLayout(new FlowLayout());
        pricePanel.add(oPriceLabel);
        pricePanel.add(oPrice);
        Container b = new Container();
        b.setLayout(new BorderLayout(10, 10));
        b.add(pricePanel, BorderLayout.EAST);

        return b;
    }

    public Container createPanelFour() {

        tFields.add(mUpLabel);
        tFields.add(mUp);
        tFields.setLayout(new FlowLayout());
        Container b = new Container();
        b.setLayout(new BorderLayout(10, 10));
        b.add(tFields, BorderLayout.EAST);

        return b;
    }

       public Container createPanelSix() {
        tArea.setLayout(new FlowLayout());
        tArea.add(area);
        area.setLineWrap(true);
        area.setEditable(false);
        Container b = new Container();
        b.setLayout(new BorderLayout(10, 10));
        b.add(tArea, BorderLayout.SOUTH);

        return b;
    }

    public Container createPanelOne() {
        rButtons.setLayout(new FlowLayout());
        
        rButtons.add(run);
        rButtons.add(hiking);
        rButtons.add(regular);

        Container c = new Container();
        c.setLayout(new BorderLayout(170, 10));
        c.add(rButtons, BorderLayout.NORTH);

        c.setVisible(true);


        return c;

    }

    public WomensF() {
        this.setBackground(Color.PINK);
        this.setLayout(new FlowLayout());
        this.setSize(600, 480);
        this.add(createPanelOne());
        this.add(createPanelSix());
        this.add(createPanelThree());
        this.add(createPanelFour());
        this.add(createPanelTwo());
        this.setVisible(true);
       // tShirts.
        calc.addActionListener(c);
        calc.addActionListener(this);
        run.addActionListener(this);
        hiking.addActionListener(this);
        regular.addActionListener(this);
        clear.addActionListener(this);
        clear.setActionCommand("clear");

    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Object row = e.getSource();
        Object r = e.getActionCommand();
              if(r == "clear")
              {
                   area.setText(null);
                   oPrice.setText(null);
                   mUp.setText(null);
                   run.setSelected(false);
                   hiking.setSelected(false);
                   regular.setSelected(false);
              }
              System.out.println(data);
              if(row == run)
                {
                   data = String.valueOf(runS);
                   oPrice.setText(data);
                   System.out.println(oPrice);
                   run.setSelected(true);
                   hiking.setSelected(false);
                   regular.setSelected(false);
            }else if(row == hiking){
                   data = String.valueOf(hikingS);
                   oPrice.setText(data);
                   System.out.println(oPrice);
                   run.setSelected(false);
                   hiking.setSelected(true);
                   regular.setSelected(false);
            
            }else if(row == regular){
                data = String.valueOf(regularS);
                   oPrice.setText(data);
                   System.out.println(oPrice);
                   run.setSelected(false);
                   hiking.setSelected(false);
                   regular.setSelected(true);
            }
        if (row == calc) {
           try{ 
            System.out.print(row);
       
            String f;
         
            t = oPrice.getText();
            c.setOCost(t);

            f = mUp.getText();
            c.setMarkDown(f);
            
            t = c.getRetail();
              area.setText("Item Name: " + item + "\nOriginal Price: $" + oPrice.getText() + "\nSale Price: $" + t + "\nDeparment: Womens' Footware");
            System.out.println(t);
    
           }catch(Exception a){
               JOptionPane.showMessageDialog(null, "The Fields cannot be empty.");
               System.out.println(a);
           }
        }
         };
    }
