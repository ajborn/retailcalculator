/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package retailcalculator;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

/**
 *
 * @author Austin
 */
public class RetailCalculator extends JFrame implements ActionListener {

    public RetailCalculator() {
        super("Retail Calculator");
        /*MensC m = new MensC();
         WomensC w = new WomensC();
         add(m);
         add(w);
         m.setVisible(false);
         w.setVisible(false);
         if(m.isVisible()){
         w.setVisible(false);
         }else if(w.isVisible())
         m.setVisible(false);
         */


    }

    public JMenuBar createBar() {
        JMenuBar mBar = new JMenuBar();
        setJMenuBar(mBar);
        //Create Menu File and add
        JMenu mFile = new JMenu("File", true);
        JMenu mClothing = new JMenu("Clothing", true);
        JMenu mFootware = new JMenu("Footware", true);
        JMenu mSports = new JMenu("Sports", true);
        JMenu mHomeK = new JMenu("Home and Kitchen", true);
        JMenu mVehicle = new JMenu("Vehicle Maintenance", true);
        mBar.add(mFile);
        mBar.add(mClothing);
        mBar.add(mFootware);
        mBar.add(mSports);
        mBar.add(mHomeK);
        mBar.add(mVehicle);
        //Create Menu Item and add    

        JMenuItem mItemExit = new JMenuItem("Exit");
        mFile.add(mItemExit);
        mItemExit.setActionCommand("Exit");
        String exit = "Exit";
        mItemExit.addActionListener(this);
        JMenuItem mMenC = new JMenuItem("Mens");
        mClothing.add(mMenC);
        mMenC.setActionCommand("Mens");
        mMenC.addActionListener(this);
        JMenuItem mWomenC = new JMenuItem("Womens");
        mClothing.add(mWomenC);
        mWomenC.setActionCommand("Womens");
        mWomenC.addActionListener(this);
        JMenuItem mMenF = new JMenuItem("Mens");
        mFootware.add(mMenF);
        mMenF.setActionCommand("Men");
        mMenF.addActionListener(this);
        JMenuItem mWomenF = new JMenuItem("Womens");
        mFootware.add(mWomenF);
        mWomenF.setActionCommand("Women");
        mWomenF.addActionListener(this);
        JMenuItem mBaseB = new JMenuItem("Baseball");
        mSports.add(mBaseB);
        mBaseB.setActionCommand("Baseball");
        mBaseB.addActionListener(this);
        JMenuItem mBaskB = new JMenuItem("Basketball");
        mSports.add(mBaskB);
        mBaskB.setActionCommand("Basketball");
        mBaskB.addActionListener(this);
        JMenuItem mFootB = new JMenuItem("Football");
        mSports.add(mFootB);
        mFootB.setActionCommand("Football");
        mFootB.addActionListener(this);
        JMenuItem mDishes = new JMenuItem("Dishes");
        mHomeK.add(mDishes);
        mDishes.setActionCommand("Dishes");
        mDishes.addActionListener(this);
        JMenuItem mSilverware = new JMenuItem("Silverware");
        mHomeK.add(mSilverware);
        mSilverware.setActionCommand("Silverware");
        mSilverware.addActionListener(this);
        JMenuItem mTires = new JMenuItem("Tires");
        mVehicle.add(mTires);
        mTires.setActionCommand("Tires");
        mTires.addActionListener(this);
        JMenuItem mCovers = new JMenuItem("Oil");
        mVehicle.add(mCovers);
        mCovers.setActionCommand("Covers");
        mCovers.addActionListener(this);


        // returns the menu bar
        return mBar;

    }

    public void actionPerformed(ActionEvent e) {

        String menuItemOpen = e.getActionCommand();
        Container[] layoutArray = new Container[11]; 
        
        MensC m = new MensC();
        layoutArray[0] = m;
        WomensC w = new WomensC();
        layoutArray[1] = w;
        MensF mf = new MensF();
        layoutArray[2] = mf;
        WomensF wf = new WomensF();
        layoutArray[3] = wf;
        Football f = new Football();
        layoutArray[4] = f;
        Baseball b = new Baseball();
        layoutArray[5] = b;
        Basketball bb = new Basketball();
        layoutArray[6] = bb;
        Dishes d = new Dishes();
        layoutArray[7] = d;
        Silverware s = new Silverware();
        layoutArray[8] = s;
        Tires t = new Tires();
        layoutArray[9] = t;
        Oil c = new Oil();
        layoutArray[10] = c;
      
                

        if (menuItemOpen == "Exit") {
            System.exit(0);
        }
        if (menuItemOpen == "Mens") {
          for(int i = 0; i < 11; i++){
                if(this.getContentPane() != layoutArray[0]){
                    this.remove(layoutArray[i]);
                }
          }
            this.setContentPane(layoutArray[0]);

            validate();
            
        }
        if (menuItemOpen == "Womens") {
             for(int i = 0; i < 11; i++){
                if(this.getContentPane() != layoutArray[1]){
                    this.remove(layoutArray[i]);
                }
          }
        
            this.setContentPane(layoutArray[1]);
            validate();
        }

        if (menuItemOpen == "Men") {
            
             for(int i = 0; i < 11; i++){
                if(this.getContentPane() != layoutArray[2]){
                    this.remove(layoutArray[i]);
                }
          }
            
            this.setContentPane(layoutArray[2]);
            validate();
        }
        if (menuItemOpen == "Women") {
             for(int i = 0; i < 11; i++){
                if(this.getContentPane() != layoutArray[3]){
                    this.remove(layoutArray[i]);
                }
          }
            this.setContentPane(layoutArray[3]);

            validate();
        }
        if (menuItemOpen == "Baseball") {
             for(int i = 0; i < 11; i++){
                if(this.getContentPane() != layoutArray[5]){
                    this.remove(layoutArray[i]);
                }
          }
            this.setContentPane(layoutArray[5]);

            validate();
        }
        if (menuItemOpen == "Basketball") {
             for(int i = 0; i < 11; i++){
                if(this.getContentPane() != layoutArray[6]){
                    this.remove(layoutArray[i]);
                }
          }
            this.setContentPane(layoutArray[6]);

            validate();
        }
        if (menuItemOpen == "Football") {
             for(int i = 0; i < 11; i++){
                if(this.getContentPane() != layoutArray[4]){
                    this.remove(layoutArray[i]);
                }
          }
            this.setContentPane(layoutArray[4]);

            validate();
        }
        if (menuItemOpen == "Dishes") {
             for(int i = 0; i < 11; i++){
                if(this.getContentPane() != layoutArray[7]){
                    this.remove(layoutArray[i]);
                }
          }
            this.setContentPane(layoutArray[7]);

            validate();
        }
        if (menuItemOpen == "Silverware") {
             for(int i = 0; i < 11; i++){
                if(this.getContentPane() != layoutArray[8]){
                    this.remove(layoutArray[i]);
                }
          }
            this.setContentPane(layoutArray[8]);

            validate();
        }
        if (menuItemOpen == "Tires") {
             for(int i = 0; i < 11; i++){
                if(this.getContentPane() != layoutArray[9]){
                    this.remove(layoutArray[i]);
                }
          }
            this.setContentPane(layoutArray[9]);

            validate();
        }
        if (menuItemOpen == "Covers") {
           for(int i = 0; i < 11; i++){
                if(this.getContentPane() != layoutArray[10]){
                    this.remove(layoutArray[i]);
                }
          }
            this.setContentPane(layoutArray[10]);

            validate();
        }
    }
    
    

    public static void main(String[] args) {
        JFrame.setDefaultLookAndFeelDecorated(false);
        RetailCalculator main = new RetailCalculator();
        final Dimension maxSize = new Dimension(450, 500);
        main.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        main.setJMenuBar(main.createBar());
        main.setSize(450, 400);
        main.setMaximumSize(maxSize);
        
        main.setVisible(true);

    }

    
}
