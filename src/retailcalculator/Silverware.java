/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package retailcalculator;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
/**
 *
 * @author Austin
 */

 public class Silverware extends JPanel implements ActionListener {

    Calculator c = new Calculator();
    private double spoonD = 3.00;
    private double forkD = 3.00;
    private double knifeD = 3.00;
    
    private String data;
    
    private JRadioButton spoon = new JRadioButton("Five Spoons:");
    private JRadioButton fork = new JRadioButton("Five Forks:");
    private JRadioButton knife = new JRadioButton("Five Knifes:");
    private String text;
    private String item;
    private String t;
    private JTextArea area = new JTextArea(text, 8,15);
    
    private JLabel oPriceLabel = new JLabel("Orginial cost of Item");

    private JLabel mUpLabel = new JLabel("Mark Down Percent: ");
    private JButton calc = new JButton("Calculate");
    private JButton clear = new JButton("Clear Fields");
    private JTextField oPrice = new JTextField(10);
    
    private JTextField mUp = new JTextField(10);
    private JPanel rButtons = new JPanel();
    private JPanel labelP = new JPanel();
    private JPanel nButtons = new JPanel();

    private JPanel pricePanel = new JPanel();
    private JPanel tArea = new JPanel();

    public Container createPanelTwo() {
        nButtons.add(calc);
        nButtons.add(clear);
        nButtons.setLayout(new FlowLayout());
        Container b = new Container();
        b.setLayout(new BorderLayout(10, 10));
        b.add(nButtons, BorderLayout.SOUTH);
        
        return b;
    }

    public Container createPanelThree() {
        pricePanel.setLayout(new FlowLayout());
        pricePanel.add(oPriceLabel);
        pricePanel.add(oPrice);
        Container b = new Container();
        b.setLayout(new BorderLayout(10, 10));
        b.add(pricePanel, BorderLayout.EAST);

        return b;
    }


    public Container createPanelFive() {
        labelP.setLayout(new FlowLayout());
        labelP.add(mUpLabel);
        labelP.add(mUp);
        Container b = new Container();
        b.setLayout(new BorderLayout(20, 10));
        b.add(labelP, BorderLayout.EAST);

        return b;
    }
       public Container createPanelSix() {
        tArea.setLayout(new FlowLayout());
        tArea.add(area);
        area.setLineWrap(true);
        area.setEditable(false);
        Container b = new Container();
        b.setLayout(new BorderLayout(10, 10));
        b.add(tArea, BorderLayout.SOUTH);

        return b;
    }

    public Container createPanelOne() {
        rButtons.setLayout(new FlowLayout());
        
        rButtons.add(spoon);
        rButtons.add(fork);
        rButtons.add(knife);

        Container c = new Container();
        c.setLayout(new BorderLayout(170, 10));
        c.add(rButtons, BorderLayout.NORTH);

        c.setVisible(true);


        return c;

    }

    public Silverware() {
         this.setBackground(Color.MAGENTA);
        this.setLayout(new FlowLayout());
        this.add(createPanelOne());
        this.add(createPanelFive());
        this.add(createPanelThree());
        this.add(createPanelSix());
        this.add(createPanelTwo());
        
        this.setVisible(true);
       // tShirts.
        calc.addActionListener(c);
        calc.addActionListener(this);
        spoon.addActionListener(this);
        fork.addActionListener(this);
        knife.addActionListener(this);
        clear.addActionListener(this);
        clear.setActionCommand("clear");

    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Object row = e.getSource();
        Object r = e.getActionCommand();
              if(r == "clear")
              {
                   
                   area.setText(null);
                   oPrice.setText(null);
                   mUp.setText(null);
                   spoon.setSelected(false);
                   fork.setSelected(false);
                   knife.setSelected(false);
              }
              System.out.println(data);
              if(row == spoon)
                {
                   data = String.valueOf(spoonD);
                   oPrice.setText(data);         
                   spoon.setSelected(true);
                   fork.setSelected(false);
                   knife.setSelected(false);
                   item = "Bowl";
            }else if(row == fork){
                   data = String.valueOf(forkD);
                   oPrice.setText(data);
                   spoon.setSelected(false);
                   fork.setSelected(true);
                   knife.setSelected(false);
                   item = "Cup";
            }else if(row == knife){
                data = String.valueOf(knifeD);
                   oPrice.setText(data);
                   spoon.setSelected(false);
                   fork.setSelected(false);
                   knife.setSelected(true);
                   item = "Plate";
            }
        if (row == calc) {
           try{ 
            System.out.print(row);
            
            String f;
           
            t = oPrice.getText();
            c.setOCost(t);

            f = mUp.getText();
            c.setMarkDown(f);
            
            t = c.getRetail();
            
           
            area.setText("Item Name: " + item + "\nOriginal Price: $" + oPrice.getText() + "\nSale Price: $" + t + "\nDeparment: Home and Kitchen");
            System.out.println(t);
    
           }catch(Exception a){
               JOptionPane.showMessageDialog(null, "The Fields cannot be empty.");
               System.out.println(a);
           }
        }
         };
    }