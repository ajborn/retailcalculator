/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package retailcalculator;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author Austin
 */
public class MensC extends JPanel implements ActionListener {

    Calculator c = new Calculator();
    private double tShirtsS = 15.00;
    private double pantsS = 25.00;
    private double underwearS = 3.00;
    private String f;
    private String data;
    private String text;
    private String t;
    private String item;
    private JTextArea area = new JTextArea(text, 10, 15);
    private JRadioButton tShirts = new JRadioButton("T-Shirts");
    private JRadioButton pants = new JRadioButton("Pants");
    private JRadioButton underwear = new JRadioButton("Underwear");
    private JLabel oPriceLabel = new JLabel("Orginial cost of Item");
    
    private JLabel mUpLabel = new JLabel("Mark Down Percent: ");
    private JButton calc = new JButton("Calculate");
    private JButton clear = new JButton("Clear Fields");
    private JTextField oPrice = new JTextField(10);
 
    
    private JTextField mUp = new JTextField(10);
    private JPanel rButtons = new JPanel();
    private JPanel labelP = new JPanel();
    private JPanel nButtons = new JPanel();
    private JPanel tFields = new JPanel();
    private JPanel pricePanel = new JPanel();
     private JPanel tArea = new JPanel();
     
    public Container createPanelTwo() {
        nButtons.add(calc);
        nButtons.add(clear);
        nButtons.setLayout(new FlowLayout());
        Container b = new Container();
        b.setLayout(new BorderLayout(10, 10));
        b.add(nButtons, BorderLayout.SOUTH);
        
        return b;
    }

    public Container createPanelThree() {
        pricePanel.setLayout(new FlowLayout());
        pricePanel.add(oPriceLabel);
        pricePanel.add(oPrice);
        Container b = new Container();
        b.setLayout(new BorderLayout(10, 10));
        b.add(pricePanel, BorderLayout.EAST);

        return b;
    }

    public Container createPanelFour() {

        tFields.add(mUpLabel);
        tFields.add(mUp);
        tFields.setLayout(new FlowLayout());
        Container b = new Container();
        b.setLayout(new BorderLayout(10, 10));
        b.add(tFields, BorderLayout.EAST);

        return b;
    }

       public Container createPanelSix() {
        tArea.setLayout(new FlowLayout());
        tArea.add(area);
        area.setLineWrap(true);
        area.setEditable(false);
        Container b = new Container();
        b.setLayout(new BorderLayout(10, 10));
        b.add(tArea, BorderLayout.SOUTH);

        return b;
    }

    public Container createPanelOne() {
        rButtons.setLayout(new FlowLayout());
        
        rButtons.add(tShirts);
        rButtons.add(pants);
        rButtons.add(underwear);

        Container c = new Container();
        c.setLayout(new BorderLayout(170, 10));
        c.add(rButtons, BorderLayout.NORTH);

        c.setVisible(true);


        return c;

    }

    public MensC() {
   
        this.setLayout(new FlowLayout());
        this.setSize(600, 480);
        this.add(createPanelOne());
        this.add(createPanelSix());
        this.add(createPanelThree());
        this.add(createPanelFour());
        this.add(createPanelTwo());
        this.setVisible(true);
       // tShirts.
        calc.addActionListener(c);
        calc.addActionListener(this);
        tShirts.addActionListener(this);
        pants.addActionListener(this);
        underwear.addActionListener(this);
        clear.addActionListener(this);
        clear.setActionCommand("clear");

    }
    @Override
    public void actionPerformed(ActionEvent e) {
        Object row = e.getSource();
        Object r = e.getActionCommand();
              if(r == "clear")
              {
                   t = null;
                   area.setText(null);
                   oPrice.setText(null);
                   mUp.setText(null);
                   tShirts.setSelected(false);
                   pants.setSelected(false);
                   underwear.setSelected(false);
              }
              System.out.println(data);
              if(row == tShirts)
                {
                   data = String.valueOf(tShirtsS);
                   oPrice.setText(data);
                   System.out.println(oPrice);
                   tShirts.setSelected(true);
                   pants.setSelected(false);
                   underwear.setSelected(false);
            }else if(row == pants){
                   data = String.valueOf(pantsS);
                   oPrice.setText(data);
                   System.out.println(oPrice);
                   tShirts.setSelected(false);
                   pants.setSelected(true);
                   underwear.setSelected(false);
            
            }else if(row == underwear){
                data = String.valueOf(underwearS);
                   oPrice.setText(data);
                   System.out.println(oPrice);
                   tShirts.setSelected(false);
                   pants.setSelected(false);
                   underwear.setSelected(true);
            }
        if (row == calc) {
            
           try{ 
            System.out.print(row);
       
            
         
            t = oPrice.getText();
            c.setOCost(t);

            f = mUp.getText();
            c.setMarkDown(f);
            item = tShirts.getText().toString();
            t = c.getRetail().toString();
            
           area.setText("Item Name: " + item + "\nOriginal Price: $" + oPrice.getText() + "\nSale Price: $" + t + "\nDepartment: Mens Clothing");
            System.out.println(t);
            
           }catch(Exception a){
               JOptionPane.showMessageDialog(null, "The Fields cannot be empty.");
               System.out.println(a);
           }
        }
         };
    }

    
        

    

